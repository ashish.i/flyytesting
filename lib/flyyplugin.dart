
import 'dart:async';

import 'package:flutter/services.dart';

class Flyyplugin {
  static const MethodChannel _channel = MethodChannel('flyyplugin');

  static Future<void> init(partnerToken)  async {
    await _channel.invokeMethod('init',{"partner_token":partnerToken});
  }
  static Future<void> setPackage(packageName)  async {
      _channel.invokeMethod('setPackage',{"package_name":packageName});
  }
  static Future<void> setUser(setUser)  async {
     _channel.invokeMethod('setUser',{"user":setUser});
  }
  static Future<void> setUserName(setUserName)  async {
     _channel.invokeMethod('setUserName',{"user_name":setUserName});
  }
  static Future<void> openOfferPage()  async {
     _channel.invokeMethod('openOffersPage');
  }
  static Future<void> handleNotification(data)  async {
    _channel.invokeMethod('handleNotification',{"notificationData":data});
  }
}
