import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flyyplugin/flyyplugin.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Flyyplugin flyy;
  late FirebaseMessaging messaging;
  late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  late AndroidNotificationChannel channel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    messaging = FirebaseMessaging.instance;
    messaging.getToken().then((value){
      print(value.toString());
    });
    flyy = Flyyplugin();
    Flyyplugin.setPackage("roidtechnologies.TempFlyyDemo.com");
    initFlyy();
    firebaseInit();
  }

  initFlyy(){
    Flyyplugin.init("4f4a13714d719106a91d");
    Flyyplugin.setUser("user435");
    Flyyplugin.setUserName("user435");
  }
  Future<void> firebaseInit() async {
    messaging = FirebaseMessaging.instance;
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    channel = const AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title// description
      importance: Importance.high,
    );
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      AppleNotification? apple = message.notification?.apple;

      if (notification != null && android != null) {
        Flyyplugin.handleNotification(message.data);
      /*  flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                icon: 'ic_android',
              ),
            ));*/
      }else if(notification != null && apple != null){

      }
      if (message.notification != null) {
        print('Notification Received');
      }
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print("opened");
      //setState(() {});
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("FLYY"),
      ),
      body: Center(
        child: Column(
          children:  [
            RaisedButton(
              onPressed: (){
                var data = {
                  "notification_source":"flyy_sdk",
                  "notification_id": "12344455",
                  "campaign_id" : "123544445",
                  "title" : "Title",
                  "message" : "hello world",
                  "deeplink":"google.com",
                  "button_text": "scratch to win",
                  "show_tray" : "true",
                  "show_inapp": "false",
                  "ref_num": "tye",
                  "reward_won":"true",
                  "reward_popup_data":"true",
                  "is_scratchable":"true",
                  "show_toast":"false"
                };
                Flyyplugin.handleNotification(data);
              },
              child: const Text("OffersPage"),
            )
          ],
        ),
      ),
    );
  }
}
