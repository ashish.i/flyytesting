import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flyyplugin/flyyplugin.dart';
import 'package:flyyplugin_example/homepage.dart';



Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  var data = {
    "notification_source":"flyy_sdk",
    "notification_id": "12344455",
    "campaign_id" : "123544445",
    "title" : "Title",
    "message" : "hello world",
    "deeplink":"google.com",
    "show_tray" : "true",
    "show_inapp": "false",
    "ref_num": "tye",
    "reward_won":"false",
    "reward_popup_data":"true",
    "is_scratchable":"true",
    "show_toast":"false"
  };
  Flyyplugin.handleNotification(data);
  print('Handling a background message ${message.messageId}');
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}
