import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flyyplugin/flyyplugin.dart';

void main() {
  const MethodChannel channel = MethodChannel('flyyplugin');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await Flyyplugin.platformVersion, '42');
  });
}
