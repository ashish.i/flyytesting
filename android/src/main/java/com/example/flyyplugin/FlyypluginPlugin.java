package com.example.flyyplugin;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;

import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import theflyy.com.flyy.Flyy;
import theflyy.com.flyy.helpers.FlyyNotificationHandler;
import theflyy.com.flyy.helpers.OnFlyyTaskComplete;

/**
 * FlyypluginPlugin
 */
public class FlyypluginPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private MethodChannel channel;
    private Activity activity;
    private Context context;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "flyyplugin");
        channel.setMethodCallHandler(this);
        context = flutterPluginBinding.getApplicationContext();
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
         if (call.method.equals("setPackage")) {
            Flyy.setPackageName(call.argument("package_name"));
        }else if (call.method.equals("init")) {
            Flyy.init(activity.getApplicationContext(),call.argument("partner_token"), 0);
        } else if (call.method.equals("setUser")) {
            Flyy.setUser(call.argument("user"), new OnFlyyTaskComplete() {
                @Override
                public void onComplete() {
                    result.success("1");
                }
            });
        } else if (call.method.equals("setUserName")) {
            Flyy.setUsername(call.argument("user_name"), new OnFlyyTaskComplete() {
                @Override
                public void onComplete() {
                    result.success("1");
                }
            });
        } else if (call.method.equals("openOffersPage")) {
            Flyy.navigateToOffersActivity(activity);
        }else if(call.method.equals("handleNotification")){
             Map<String,String> notificationData = call.argument("notificationData");
             System.out.println(notificationData.get("title"));
             Flyy.showRewardWonPopup(context, notificationData.get("title"), notificationData.get("message"), notificationData.get("deeplink"),notificationData.get("button_text"),true);
            // FlyyNotificationHandler.handleCrossPlatformNotification(context,notificationData,null,null);
         }
    }

    @Override
    public void onAttachedToActivity(ActivityPluginBinding activityPluginBinding) {
        // TODO: your plugin is now attached to an Activity
        activity = activityPluginBinding.getActivity();
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        // TODO: the Activity your plugin was attached to was
        // destroyed to change configuration.
        // This call will be followed by onReattachedToActivityForConfigChanges().
    }

    @Override
    public void onReattachedToActivityForConfigChanges(ActivityPluginBinding activityPluginBinding) {
        // TODO: your plugin is now attached to a new Activity
        // after a configuration change.
    }

    @Override
    public void onDetachedFromActivity() {
        // TODO: your plugin is no longer associated with an Activity.
        // Clean up references.
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }
}
